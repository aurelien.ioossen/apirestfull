package dao;

import job.Personne;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author IOOSSEN AURELIEN created on 28/08/2019
 */


public class PersonneDAO extends DAO
{
    public PersonneDAO(Connection connection)
    {
        super(connection);
    }

    @Override
    public <T> T getByID(int id)
    {
        Personne personne = null ;

        try
        {
            String request = " SELECT * FROM T_PERSONNE WHERE PRS_ID= "+id;
            PreparedStatement preparedStatement = connection.prepareStatement(request);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            while (rs.next())
            {

                personne = new Personne(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));


            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }


        return (T) personne;
    }

    @Override
    public <T> T getByID(String id)
    {
        return null;
    }

    @Override
    public <T> ArrayList<T> getAll()
    {
        ArrayList<Personne> listPersonne = new ArrayList<>();
        Personne personne = new Personne(0, "", "", "");

        try
        {
            String request = " SELECT * FROM T_PERSONNE ";
            PreparedStatement preparedStatement = connection.prepareStatement(request);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            while (rs.next())
            {

                personne = new Personne(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
                listPersonne.add(personne);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }


        return (ArrayList<T>) listPersonne;
    }

    @Override
    public boolean insert(Object o)
    {
        return false;
    }

    @Override
    public boolean delete(Object o)
    {
        return false;
    }

    @Override
    public boolean update(Object o)
    {
        return false;
    }


}
