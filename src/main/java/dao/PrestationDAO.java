package dao;


import job.Prestation;
import job.Prestation_Gite;
import job.Sous_type_Prestation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author IOOSSEN AURELIEN created on 28/08/2019
 */
public class PrestationDAO extends DAO
{
    public PrestationDAO(Connection connection)
    {
        super(connection);
    }

    @Override
    public <T> T getByID(int id)
    {
        Prestation prestation = new Prestation(0, "");
        String request = "SELECT * FROM V_PRESTATION WHERE PST_ID= " + id;

        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(request);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            while (rs.next())
            {

                prestation = new Prestation(rs.getInt(0), rs.getString(2));

            }

            rs.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return (T) prestation;
    }

    @Override
    public <T> T getByID(String id)
    {
        return null;
    }

    @Override
    public <T> ArrayList<T> getAll()
    {
        ArrayList listPrestation = new ArrayList();
        Prestation prestation = new Prestation(0, "");
        String request = "SELECT * FROM V_PRESTATION ORDER BY PST_ID";

        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(request);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            while (rs.next())
            {

                prestation = new Prestation(rs.getInt(1), rs.getString(2));
                prestation.setSousTypePrestation(new Sous_type_Prestation(rs.getInt(3), rs.getString(4)));
                listPrestation.add(prestation);
            }

            rs.close();
        }
        catch (SQLException e)
        {

            e.printStackTrace();
        }


        return listPrestation;
    }


    ArrayList<Prestation_Gite> getAllByGite(int id_gite)
    {
        ArrayList<Prestation_Gite> listPrestationGite = new ArrayList<>();
        ResultSet rs;
        try
        {

            String request = "SELECT * FROM T_PRESTATION " + "INNER JOIN T_PROPOSER_PRESTATION TPP on T_PRESTATION.PST_ID = TPP.PRP_PRESTATION_ID " + "INNER JOIN T_GITE TG on TPP.PRP_GITE_ID = TG.GITE_ID WHERE GITE_ID=?";
            PreparedStatement pstmt = connection.prepareStatement(request);
            pstmt.setInt(1, id_gite);
            pstmt.execute();
            rs = pstmt.getResultSet();
            while (rs.next())
            {

                Prestation_Gite prestation = new Prestation_Gite(new Prestation(rs.getInt(1), rs.getString(2)), rs.getInt(4));
                listPrestationGite.add(prestation);
                System.out.println("ajout de la prestation :"+prestation +" au gite");

            }
            rs.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return listPrestationGite;
    }


    @Override
    public boolean insert(Object o)
    {
        return false;
    }

    @Override
    public boolean delete(Object o)
    {
        return false;
    }

    @Override
    public boolean update(Object o)
    {
        return false;
    }


}
