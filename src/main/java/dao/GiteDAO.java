package dao;


import job.Gite;
import job.Prestation;
import job.Prestation_Gite;

import java.sql.*;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 * @author IOOSSEN AURELIEN & WOJTOWICZ DENIS created on 28/08/2019
 */


public class GiteDAO extends DAO
{
    private Gite gite;

    public GiteDAO(Connection connection)
    {
        super(connection);
    }

    @Override
    public <T> T getByID(int id)
    {
        Gite gite = new Gite(0, "");
        try
        {
            String request = "SELECT * FROM T_GITE where GITE_ID =   " + id;
            PreparedStatement preparedStatement = connection.prepareStatement(request);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            while (rs.next())
            {

                gite = new Gite(rs.getInt(1), rs.getString(2));
                gite.setSurfaceHabitable(rs.getInt(3));
                gite.setNmbCouchage(rs.getString(4));
                gite.setTarif(rs.getString(6));
                gite.setComplementGeo(rs.getString(7));
                gite.setComplementAdresse(rs.getString(8));
                gite.setNumero(rs.getString(9));
                gite.setRue(rs.getString(10));
                gite.setLieuDit(rs.getString(11));
                gite.setContact(DAOFactory.getPersonneDAO().getByID(rs.getInt(13)));
                gite.setProprietaire(DAOFactory.getPersonneDAO().getByID(rs.getInt(14)));
                gite.setVille(DAOFactory.getCityDAO().getByID(rs.getString(12)));
                gite.setListePrestation(generatePrestationList(gite));

            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }


        return (T) gite;
    }

    @Override
    public <T> T getByID(String id)
    {
        return null;
    }

    @Override
    public <T> ArrayList<T> getAll()
    {
        ArrayList listGite = new ArrayList();
        Gite gite = new Gite(0, "");
        try
        {
            String request = "SELECT * FROM T_GITE ";
            PreparedStatement preparedStatement = connection.prepareStatement(request);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            while (rs.next())
            {

                gite = new Gite(rs.getInt(1), rs.getString(2));

                gite.setSurfaceHabitable(rs.getInt(3));
                gite.setNmbCouchage(rs.getString(4));
                gite.setNmbChambre(rs.getString(5));
                gite.setTarif(rs.getString(6));
                gite.setComplementGeo(rs.getString(7));
                gite.setComplementAdresse(rs.getString(8));
                gite.setNumero(rs.getString(9));
                gite.setRue(rs.getString(10));
                gite.setLieuDit(rs.getString(11));
                gite.setContact(DAOFactory.getPersonneDAO().getByID(rs.getInt(13)));
                gite.setProprietaire(DAOFactory.getPersonneDAO().getByID(rs.getInt(14)));
                gite.setVille(DAOFactory.getCityDAO().getByID(rs.getString(12)));
                gite.setListePrestation(generatePrestationList(gite));
                listGite.add(gite);

            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }


        return (ArrayList<T>) listGite;
    }



    private String convertListBenefitsToString(ArrayList<Prestation> listBenefit)
    {
        StringBuilder listIdBenefits = new StringBuilder();

        ListIterator itr = listBenefit.listIterator();
        while (itr.hasNext())
        {
            if (itr.hasPrevious())
            {
                listIdBenefits.append(",");
            }

            Prestation pst = (Prestation) itr.next();
            String id = String.valueOf(pst.getId_prestation());
            listIdBenefits.append(id);


        }

        System.out.println(listIdBenefits.toString());
        return listIdBenefits.toString();


    }


    /**
     * @param gite
     * @return List of benefits gites
     */

    public ArrayList<Prestation_Gite> generatePrestationList(Gite gite)
    {
        return DAOFactory.getPrestationDAO().getAllByGite(gite.getIdGite());

    }

    @Override
    public boolean insert(Object o)
    {
        Gite gite = (Gite) o;
        try
        {

            String requestInsertion = "INSERT INTO T_GITE(GITE_NOM, GITE_SURFACE, GITE_NMBRE_COUCHAGE, GITE_NMBRE_CHAMBRE, GITE_TARIF_PROPRIETAIRE," + " GITE_COMPLEMENT_ADRESSE, GITE_COMPLEMENT_GEOGRAPHIQUE, GITE_NUMERO, GITE_LIBELLE_RUE, GITE_LIEU_DIT, GITE_NUM_INSEE," + " GITE_ID_CONTACT, GITE_ID_PROPRIETAIRE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pstmt = connection.prepareStatement(requestInsertion);

            pstmt.setString(1, gite.getLibelleGite());
            pstmt.setInt(2, gite.getSurfaceHabitable());
            pstmt.setInt(3, gite.getNmbCouchage());
            pstmt.setInt(4, gite.getNmbChambre());
            pstmt.setBigDecimal(5, gite.getTarif());
            pstmt.setString(6, gite.getComplementAdresse());
            pstmt.setString(7, gite.getComplementGeo());
            pstmt.setString(8, gite.getNumero());
            pstmt.setString(9, gite.getRue());
            pstmt.setString(10, gite.getLieuDit());
            pstmt.setString(11, gite.getVille().getNumInsee());
            pstmt.setInt(12, gite.getContact().getId_personne());
            pstmt.setInt(13, gite.getProprietaire().getId_personne());
            pstmt.execute();

            int idGiteinserted = getIdGiteInserted();

            ArrayList<Prestation_Gite> listPrestationGite = gite.getListePrestation();
            for (Prestation_Gite pstGite : listPrestationGite)
            {
                String requestInsertPrestation = "INSERT INTO T_PROPOSER_PRESTATION(PRP_PRIX, PRP_GITE_ID, PRP_PRESTATION_ID) VALUES (?,?,?)";
                PreparedStatement pstmtPrestation = connection.prepareStatement(requestInsertPrestation);
                pstmtPrestation.setInt(1, pstGite.getPrix_prestation());
                pstmtPrestation.setInt(2, idGiteinserted);
                pstmtPrestation.setInt(3, pstGite.getPrestation().getId_prestation());
                pstmtPrestation.execute();
            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    public int getIdGiteInserted() throws SQLException
    {
        int id = 0;
        String requestGetidGite = "SELECT TOP 1 GITE_ID FROM T_GITE ORDER BY GITE_ID DESC";
        PreparedStatement pstmtId = connection.prepareStatement(requestGetidGite);
        pstmtId.execute();
        ResultSet rs = pstmtId.getResultSet();
        while (rs.next())
        {
            id = rs.getInt(1);
        }
        return id;
    }

    @Override
    public boolean delete(Object o)
    {
        System.out.println("Entrez dans la methoed delete");
        Gite gite = (Gite) o;
        boolean isQueryExecuted =false;
        try
        {

            String request = "DELETE FROM T_GITE WHERE GITE_ID = ?";
            PreparedStatement pstmt = connection.prepareStatement(request);
            pstmt.setInt(1, gite.getIdGite());
            pstmt.execute();
            System.out.println("gite supprimé");
            isQueryExecuted = true ;

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return isQueryExecuted;
    }

    @Override
    public boolean update(Object o)
    {
        boolean isQueryExecuted =false ;
        Gite gite = (Gite) o;

        System.out.println("Entrez dans la methoed UPDATE :"+o.toString());
        String request = "UPDATE T_GITE SET GITE_NOM = ?,GITE_SURFACE = ?,GITE_NMBRE_CHAMBRE=?,GITE_NMBRE_COUCHAGE=?," +
                "GITE_TARIF_PROPRIETAIRE=?,GITE_COMPLEMENT_ADRESSE=?,GITE_COMPLEMENT_GEOGRAPHIQUE=?," +
                "GITE_NUMERO=?,GITE_LIBELLE_RUE=?,GITE_LIEU_DIT=?,GITE_NUM_INSEE=?,GITE_ID_CONTACT=?,GITE_ID_PROPRIETAIRE=?" +
                " WHERE GITE_ID=?";

        PreparedStatement pstmt = null;
        try
        {
            pstmt = connection.prepareStatement(request);
            pstmt.setString(1, gite.getLibelleGite());
            pstmt.setInt(2, gite.getSurfaceHabitable());
            pstmt.setInt(4, gite.getNmbChambre());
            pstmt.setInt(3, gite.getNmbCouchage());
            pstmt.setBigDecimal(5, gite.getTarif());
            pstmt.setString(6, gite.getComplementAdresse());
            pstmt.setString(7, gite.getComplementGeo());
            pstmt.setString(8, gite.getNumero());
            pstmt.setString(9, gite.getRue());
            pstmt.setString(10, gite.getLieuDit());
            pstmt.setString(11, gite.getVille().getNumInsee());
            pstmt.setInt(12, gite.getContact().getId_personne());
            pstmt.setInt(13, gite.getProprietaire().getId_personne());
            pstmt.execute();
            isQueryExecuted = true;
        }

        catch (SQLException e)
        {
            e.printStackTrace();
        }



        return isQueryExecuted;
    }

}