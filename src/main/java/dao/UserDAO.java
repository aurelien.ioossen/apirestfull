package dao;

import job.Personne;
import job.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author IOOSSEN AURELIEN created on 28/08/2019
 */


public class UserDAO extends DAO
{
    public UserDAO(Connection connection)
    {
        super(connection);
    }

    @Override
    public <T> T getByID(int id)
    {
        return null;
    }

    @Override
    public <T> T getByID(String id)
    {
        return null;
    }

    @Override
    public <T> ArrayList<T> getAll()
    {
        return null;
    }

    @Override
    public boolean insert(Object o)
    {
        return false;
    }

    @Override
    public boolean delete(Object o)
    {
        return false;
    }

    @Override
    public boolean update(Object o)
    {
        return false;
    }


    public <T> T getByLogin(String login)
    {
        User user = null;

        try
        {
            String request = " SELECT * FROM T_USER WHERE USR_LOGIN = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(request);
            preparedStatement.setString(1,login);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getResultSet();
            while (rs.next())
            {

                user = new User(rs.getString(1),rs.getString(2));


            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }


        return (T) user;
    }



}
