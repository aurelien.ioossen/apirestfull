package dao;



import job.Departement;
import job.Region;
import job.Ville;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DepartementDAO extends DAO
{

    /**
     * @author IOOSSEN AURELIEN created on 28/08/2019
     */

    public DepartementDAO(Connection connection)
    {
        super(connection);
    }

    @Override
    public <T> T getByID(int id)
    {
        Departement departement = new Departement("0", "");
        Ville ville = new Ville("0", "");


        ResultSet rs;
        try
        {
            Statement stmt = connection.createStatement();
            String request = "SELECT * FROM V_LOCALITE WHERE DPT_NUM=" + id;
            rs = stmt.executeQuery(request);

            while (rs.next())
            {
                if (!rs.getString(3).equals(departement.getNumeroDepartement()))
                {

                    departement = new Departement(rs.getString(1), rs.getString(2));
                    departement.setRegion(new Region(rs.getString(1), rs.getString(2)));
                }
                ville = new Ville(rs.getString(5), rs.getString(6));
                ville.setLatitude(rs.getFloat(7));
                ville.setLongitude(rs.getFloat(8));
                ville.setCodePostal(rs.getString(9));
                departement.addVille(ville);

            }
            rs.close();

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return (T) departement;
    }

    @Override
    public <T> T getByID(String id)
    {
        return null;
    }

    @Override
    public <T> ArrayList<T> getAll()
    {
        ArrayList<Departement> listDepartement = new ArrayList<>();
        Departement departement = new Departement("0", "");
        Ville ville = new Ville("0", "");

        ResultSet rs;
        try
        {
            Statement stmt = connection.createStatement();
            String request = "SELECT * FROM V_LOCALITE ORDER BY DPT_NUM";
            rs = stmt.executeQuery(request);

            while (rs.next())
            {
                if (!rs.getString(3).equals(departement.getNumeroDepartement()))
                {

                    departement = new Departement(rs.getString(3), rs.getString(4));
                    departement.setRegion(new Region(rs.getString(1), rs.getString(2)));
                    listDepartement.add(departement);
                }
                ville = new Ville(rs.getString(5), rs.getString(6));
                ville.setLatitude(rs.getFloat(7));
                ville.setLongitude(rs.getFloat(8));
                ville.setCodePostal(rs.getString(9));
                departement.addVille(ville);


            }


            rs.close();

        }
        catch (SQLException e)
        {

            e.printStackTrace();
        }

        return (ArrayList<T>) listDepartement;
    }

    @Override
    public boolean insert(Object o)
    {
        return false;
    }

    @Override
    public boolean delete(Object o)
    {
        return false;
    }

    @Override
    public boolean update(Object o)
    {
        return false;
    }


}
