package dao;

import java.sql.Connection;

public class DAOFactory
{

    /**
     * @author IOOSSEN AURELIEN & WOJTOWICZ DENIS created on 27/08/2019
     */
    private static final Connection connection = PROGICAConnect.getInstance();



    public static RegionDAO getRegionDAO()
    {
        return new RegionDAO(connection);
    }

    public static DepartementDAO getDepartmentDAO()
    {
        return new DepartementDAO(connection);
    }

    public static VilleDAO getCityDAO()
    {
        return new VilleDAO(connection);
    }

    public static PrestationDAO getPrestationDAO()
    {
        return new PrestationDAO(connection);
    }

    public static PersonneDAO getPersonneDAO()
    {
        return new PersonneDAO(connection);
    }

    public static GiteDAO getGiteDAO()
    {
        return new GiteDAO(connection);
    }

    public static UserDAO getUserDAO(){
    return new UserDAO(connection) ;
}

    public static DisponibiliteDAO getDisponibiliteDAO()
    {

        return new DisponibiliteDAO(connection);
    }

}
