package dao;

import job.Region;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class RegionDAO extends DAO
{

    /**
     * @author IOOSSEN AURELIEN created on 28/08/2019
     */

    public RegionDAO(Connection connection)
    {
        super(connection);
    }

    @Override
    public <T> T getByID(int id)
    {
        Region region = new Region("0", "");

        ResultSet rs;
        try
        {
            Statement stmt = connection.createStatement();
            String request = "SELECT * FROM V_LOCALITE  WHERE RGN_CODE=" + id + " ORDER BY RGN_CODE";
            rs = stmt.executeQuery(request);
            while (rs.next())
            {
                if (region.getCode_region() != rs.getString(1))
                {
                    region = new Region(rs.getString(1), rs.getString(2));
                }
            }
            rs.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return (T) region;
    }

    @Override
    public <T> T getByID(String id)
    {
        return null;
    }

    public <T> ArrayList<T> getAll()

    {

        System.out.println("ENTREZ DANS LE GETALL");

        ArrayList<Region> listeRegion = new ArrayList<>();
        Region lastRegionCreated = new Region("0", "");
        ResultSet rs;
        try
        {
            Statement stmt = connection.createStatement();
            String request = "SELECT * FROM V_LOCALITE  ORDER BY RGN_CODE,DPT_NUM";
            rs = stmt.executeQuery(request);
            while (rs.next())
            {
                if (!lastRegionCreated.getCode_region().equals(rs.getString(1)))
                {
                    lastRegionCreated = new Region(rs.getString(1), rs.getString(2));
                    listeRegion.add(lastRegionCreated);
                }
            }


            rs.close();

        }
        catch (SQLException e)
        {

            e.printStackTrace();
        }

        return (ArrayList<T>) listeRegion;


    }

    public boolean insert(Object o)
    {
        return false;
    }

    public boolean delete(Object o)
    {
        return false;
    }

    public boolean update(Object o)
    {
        return false;
    }


}
