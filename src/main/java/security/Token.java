package security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import job.Login;


import java.util.Calendar;
import java.util.Date;

/**
 * @author IOOSSEN AURELIEN created on 30/10/2019
 */


public class Token
{
    private String token;
    private Algorithm algorithm = Algorithm.HMAC256("maCleSecreteAmoi");


    public Token(String tokenHeader)
    {
        if (tokenHeader != null && tokenHeader.startsWith("Bearer "))
        {
            token = tokenHeader.substring(7);
        }
        else
        {
            token = "";
        }


    }


    public Token(Login login)
    {

        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        calendar.add(Calendar.MINUTE, 1);
        Date expiration = calendar.getTime();
        token = JWT.create().withClaim("login", login.getLogin()).withIssuedAt(now).withExpiresAt(expiration).sign(algorithm);


    }


    public boolean isValide()
    {
        boolean isValide = false;
        Calendar calendar = Calendar.getInstance();
        Date dateNow = calendar.getTime();
        JWTVerifier jwtVerifier = JWT.require(algorithm).build();

        try
        {
            DecodedJWT decodedJWT = jwtVerifier.verify(token);
            isValide = decodedJWT.getExpiresAt().after(dateNow);
        }
        catch (Exception e)
        {

        }
        return isValide;

    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }
}
