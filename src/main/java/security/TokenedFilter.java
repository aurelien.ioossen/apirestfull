package security;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

import static security.Tokened.TOKEN;

/**
 *  @author IOOSSEN AURELIEN created on 30/10/2019
 *
 */

@Provider
@Tokened
@Priority(Priorities.AUTHENTICATION)




public class TokenedFilter implements ContainerRequestFilter
{


    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException
    {
        System.out.println("FILTER");


        String tokenHeader = containerRequestContext.getHeaderString(TOKEN);
        Token token = new Token(tokenHeader);
        if (!token.isValide()){
            containerRequestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());

        }
    }
}
