package rest;

import dao.DAOFactory;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import job.Region;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * @author IOOSSEN AURELIEN created on 29/10/2019
 */

@Path("/regions")
@Tag(name = "Régions")
@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})

public class CouleurRest
{


    @GET
    @Operation(summary = "retourne la liste des Régions")
    public Response getRegions()
    {
        ArrayList<Region> listRegion = DAOFactory.getRegionDAO().getAll();
        if (listRegion == null)
        {
            return Response.noContent().build();
        }
        else
        {
            return Response.ok(new GenericEntity<List<Region>>(listRegion)
            {
            }).build();
        }
    }


    @GET
    @Operation(summary = "retourne la Région pour l'ID concerné")
    @Path("{id}")
    public Response getRegionByID(@PathParam("id") Integer idGite)
    {
        Region region = DAOFactory.getRegionDAO().getByID(idGite);
        return Response.ok(new GenericEntity<Region>(region)
        {
        }).build();


    }


}
