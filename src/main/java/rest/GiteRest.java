package rest;

import dao.DAOFactory;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import job.Gite;
import security.Tokened;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/gites")
@Tag(name = "Gites")
@Produces(MediaType.APPLICATION_JSON)

public class GiteRest
{

    /**
     * @author IOOSSEN AURELIEN created on 29/10/2019
     */


    @GET
    @Tokened
    @Operation(summary = "Retourne la liste eds Gites")
    public Response getGites()
    {

        ArrayList<Gite> listGites = DAOFactory.getGiteDAO().getAll();
        if (listGites == null)
        {
            return Response.noContent().build();
        }

        return Response.ok(new GenericEntity<List<Gite>>(listGites)
        {
        }).build();


    }


    @GET
    @Operation(summary = "retourne le gite correspondant à l'id")
    @Path("{idGite}")
    public Response getGiteById(@PathParam("idGite") Integer idGite)
    {

        Gite gite = DAOFactory.getGiteDAO().getByID(idGite);
        if (gite == null)
        {
            return Response.noContent().build();
        }
        return Response.ok(new GenericEntity<Gite>(gite)
        {
        }).build();


    }

    @DELETE
    @Operation(summary = "Suppression du gite correspondant")
    @Path("{idGite}")
    public Response deleteGiteById(@PathParam("idGite") Integer idGite)
    {

        if (!DAOFactory.getGiteDAO().delete(DAOFactory.getGiteDAO().getByID(idGite)))
        {

            return Response.serverError().build();
        }
        else
        {

            return Response.ok().build();
        }


    }

    @PUT
    @Operation(summary = "Mise à jour du Gite correspondant")
    @Path("{idGite}")
    public Response updateGiteById(@PathParam("idGite") Integer idGite, Gite gite)
    {

        if (!DAOFactory.getGiteDAO().update(gite))
        {
            return Response.ok().build();

        }
        else
        {

            return Response.serverError().build();
        }

    }

}
