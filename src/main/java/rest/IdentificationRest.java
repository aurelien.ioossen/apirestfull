package rest;

import dao.DAOFactory;
import io.swagger.v3.oas.annotations.tags.Tag;
import job.Login;
import job.User;
import org.mindrot.jbcrypt.BCrypt;
import security.Token;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.*;

import static security.Tokened.TOKEN;

@Path("/identification")
@Tag(name = "Identification")

@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class IdentificationRest
{

    /**
     *  @author IOOSSEN AURELIEN created on 30/10/2019 
     *
     */


    @POST
    public Response insert(Login login){
        System.out.println("login testé :"+login.getLogin());
        if (login == null){

            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        User user = DAOFactory.getUserDAO().getByLogin(login.getLogin()) ;
        System.out.println("Utilisateur trouvé "+user);

        if (BCrypt.checkpw(login.getPassword(),user.getPassword())){
            System.out.println("mot de passe conforme");
            return Response.ok().header(TOKEN,"Bearer "+new Token(login).getToken()).build();

        }else {

            System.out.println("MOT DE PASSE INCORRECT");
            return Response.noContent().build();
        }
    }



}
