package job;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author IOOSSEN AURELIEN created on 27/08/2019
 */

@XmlRootElement(name = "region")
public class Region
{

    private String code_region;
    private String nom_region;
//    private ArrayList<Departement> listeDepartement;


    public Region(String code_region, String nom_region)
    {
        this.code_region = code_region;
        this.nom_region = nom_region;


    }

    public Region(){}


    public String getCode_region()
    {
        return code_region;
    }

    public void setCode_region(String code_region)
    {
        this.code_region = code_region;
    }

    public String getNom_region()
    {
        return nom_region;
    }

    public void setNom_region(String nom_region)
    {
        this.nom_region = nom_region;
    }



//    @Override
//    public String toString()
//    {
//        return nom_region;
//    }


}
