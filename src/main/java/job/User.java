package job;

public class User
{

    /**
     *  @author IOOSSEN AURELIEN created on 30/10/2019 
     *
     */


    private String name;
    private String password;

    public User(String name, String password)
    {
        this.name = name;
        this.password = password;
    }


    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }


    @Override
    public String toString()
    {
        return "User{" + "name='" + name + '\'' + ", password='" + password + '\'' + '}';
    }
}
