package job;

public class Login
{

    /**
     * @author IOOSSEN AURELIEN created on 30/10/2019
     */


    private String login;
    private String password;

    public Login()
    {
    }


    public Login(String login, String password)
    {
        this.login = login;
        this.password = password;
    }


    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
}
