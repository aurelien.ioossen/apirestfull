package job;

public class Disponibilite
{

    /**
     * @author IOOSSEN AURELIEN created on 27/08/2019
     */

    private int id_disponibilite;
    private String jour_debut;
    private String jour_fin;
    private String heure_debut;
    private String heure_fin;

    public Disponibilite(int id_disponibilite, String jour_debut, String jour_fin, String heure_debut, String heure_fin)
    {
        this.id_disponibilite = id_disponibilite;
        this.jour_debut = jour_debut;
        this.jour_fin = jour_fin;
        this.heure_debut = heure_debut;
        this.heure_fin = heure_fin;
    }



    public int getId_disponibilite()
    {
        return id_disponibilite;
    }

    public void setId_disponibilite(int id_disponibilite)
    {
        this.id_disponibilite = id_disponibilite;
    }

    public String getJour_debut()
    {
        return jour_debut;
    }

    public void setJour_debut(String jour_debut)
    {
        this.jour_debut = jour_debut;
    }

    public String getJour_fin()
    {
        return jour_fin;
    }

    public void setJour_fin(String jour_fin)
    {
        this.jour_fin = jour_fin;
    }

    public String getHeure_debut()
    {
        return heure_debut;
    }

    public void setHeure_debut(String heure_debut)
    {
        this.heure_debut = heure_debut;
    }

    public String getHeure_fin()
    {
        return heure_fin;
    }

    public void setHeure_fin(String heure_fin)
    {
        this.heure_fin = heure_fin;
    }

    @Override
    public String toString()
    {
        String dispo;
        if (jour_debut != jour_fin)
        {
            dispo = "Du "+jour_debut +" au "+jour_fin+ " ( " + heure_debut +"H à " + heure_fin + "H ) ";
        }
        else
        {
            dispo = jour_debut + " ( " + heure_debut + "H à " + heure_fin + "H ) ";
        }
        return dispo;
    }
}
