package job;

import java.util.Date;

public class Periode
{

    /**
     *  @author IOOSSEN AURELIEN created on 27/08/2019 
     *
     */

    private int id_periode;
    private Date date_debut;
    private Date date_fin;
    private Type_periode type_periode;


    public Periode(int id_periode, Date date_debut, Date date_fin)
    {
        this.id_periode = id_periode;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
    }


    public int getId_periode()
    {
        return id_periode;
    }

    public void setId_periode(int id_periode)
    {
        this.id_periode = id_periode;
    }

    public Date getDate_debut()
    {
        return date_debut;
    }

    public void setDate_debut(Date date_debut)
    {
        this.date_debut = date_debut;
    }

    public Date getDate_fin()
    {
        return date_fin;
    }

    public void setDate_fin(Date date_fin)
    {
        this.date_fin = date_fin;
    }

    public Type_periode getType_periode()
    {
        return type_periode;
    }

    public void setType_periode(Type_periode type_periode)
    {
        this.type_periode = type_periode;
    }
}
