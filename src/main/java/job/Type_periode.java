package job;

import java.util.ArrayList;

public class Type_periode
{

    /**
     * @author IOOSSEN AURELIEN created on 27/08/2019
     */


    private int id_type_periode;
    private String libelle_type;
    private ArrayList<Periode> listePeriode;

    public Type_periode(int id_type_periode, String libelle_type)
    {
        this.id_type_periode = id_type_periode;
        this.libelle_type = libelle_type;
    }


    public int getId_type_periode()
    {
        return id_type_periode;
    }

    public void setId_type_periode(int id_type_periode)
    {
        this.id_type_periode = id_type_periode;
    }

    public String getLibelle_type()
    {
        return libelle_type;
    }

    public void setLibelle_type(String libelle_type)
    {
        this.libelle_type = libelle_type;
    }


    public ArrayList<Periode> getListePeriode()
    {
        return listePeriode;
    }

    public void setListePeriode(ArrayList<Periode> listePeriode)
    {
        this.listePeriode = listePeriode;
    }




}
